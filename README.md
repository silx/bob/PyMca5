# PyMca5

[![pipeline status](https://gitlab.esrf.fr/silx/bob/PyMca5/badges/master/pipeline.svg)](https://gitlab.esrf.fr/silx/bob/PyMca5/pipelines)

Build binary packages for [PyMca5](https://github.com/vasole/pymca): [Artifacts](https://silx.gitlab-pages.esrf.fr/bob/PyMca5)
